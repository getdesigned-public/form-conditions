const path = require('path');

module.exports = {
    mode: 'production',
    entry: {
        main: {
            import: './Resources/Private/JavaScript/Module.js',
            filename: 'JavaScript/form-conditions.js',
            library: {
                name: 'conditionMatcher',
                type: 'window',
                export: 'default'
            }
        }
    },
    output: {
        path: path.resolve(__dirname, 'Resources/Public')
    },
    module: {
        rules: [
            {
                test: /\.js$/i,
                exclude: /(node_modules)/,
                use: [
                    {
                        loader: 'babel-loader',
                        options: {
                            presets: ['@babel/preset-env']
                        }
                    }
                ]
            }
        ]
    }
};