<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'Conditional Form Elements',
    'description' => 'Introduces the activation of form elements depending on the current form values',
    'category' => 'fe',
    'version' => '12.1.1-dev',
    'state' => 'stable',
    'author' => 'Daniel Haring',
    'author_company' => 'Getdesigned GmbH',
    'author_email' => 'dh@getdesigned.at',
    'constraints' => [
        'depends' => [
            'core' => '12.4.0-12.99.99',
            'form' => '12.4.0-12.99.99'
        ]
    ]
];