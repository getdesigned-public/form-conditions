<?php

declare(strict_types=1);
namespace Getdesigned\FormConditions\Service;

/*
 * Copyright notice
 *
 * (c) 2025 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Http\ApplicationType;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\FormElementInterface;
use TYPO3\CMS\Form\Domain\Model\FormElements\Page;
use TYPO3\CMS\Form\Domain\Model\Renderable\RenderableInterface;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;

/**
 * Manager for handling form conditions.
 *
 * @since 12.1.0
 * @author Daniel Haring <dh@getdesigned.at>
 * @package Form Conditions
 */
class FormConditionManager implements SingletonInterface
{
    /**
     * The conditions registered.
     *
     * @var array[]
     */
    protected array $conditions = [];

    /**
     * Registry of foreign page values.
     *
     * @var array[]
     */
    protected array $foreignValues = [];

    /**
     * Registry of styles to apply.
     *
     * @var array
     */
    protected array $styles = [];

    /**
     * Registers an elements conditions.
     *
     * @param FormElementInterface $formElement The element to register the conditions of
     * @param FormRuntime $formRuntime The corresponding form runtime
     * @return void
     */
    public function addElement(FormElementInterface $formElement, FormRuntime $formRuntime): void
    {
        if (
            !ApplicationType::fromRequest($formRuntime->getRequest())->isFrontend()
            || empty($enableCondition = (string)($formElement->getProperties()['enableCondition'] ?? ''))
        ) {
            return;
        }

        foreach (GeneralUtility::trimExplode(';', $enableCondition, true) as $index => $composite) {
            foreach (GeneralUtility::trimExplode(',', $composite, true) as $condition) {
                if (
                    !preg_match('/^(?<identifier>[^!=<>]+)(?<operator>[!=<>]+)?(?<value>.*)$/', $condition, $cmp)
                    || empty($targetElement = $formRuntime->getFormDefinition()->getElementByIdentifier($cmp['identifier']))
                ) {
                    continue;
                }
                $this->styles[] = '.form-group:has(#' . $formElement->getUniqueIdentifier() . '):not(.tx-form-conditions-display){display:none;}';
                $this->conditions[$formElement->getUniqueIdentifier()][$index][$targetElement->getUniqueIdentifier()] = [
                    'operator' => $cmp['operator'] ?: null,
                    'comparison' => $cmp['value'] ?? null
                ];
                    // provide value of target element if it's on a different page
                if (
                    !empty($sourcePage = $this->getPageFromElement($formElement))
                    && !empty($targetPage = $this->getPageFromElement($targetElement))
                    && $sourcePage->getIdentifier() !== $targetPage->getIdentifier()
                ) {
                    $targetValue = $formRuntime[$targetElement->getIdentifier()];
                    if (!is_string($targetValue)) {
                        $targetValue = empty($targetValue) ? '0' : '1';
                    }
                    $this->foreignValues[$targetElement->getUniqueIdentifier()] = $targetValue;
                }
            }
        }
    }

    /**
     * Returns and resets the currently registered inline settings.
     *
     * @return array[] The currently registered inline Settings
     */
    public function flushInlineSettings(): array
    {
        $settings = [];

        if (!empty($this->foreignValues)) {
            $settings['ext']['form_conditions']['val'] = $this->foreignValues;
            $this->foreignValues = [];
        }

        if (!empty($this->conditions)) {
            $settings['ext']['form_conditions']['cond'] = $this->conditions;
            $this->conditions = [];
        }

        return $settings;
    }

    /**
     * Returns and resets the currently registered styles.
     *
     * @return string The currently registered styles
     */
    public function flushInlineStyles(): string
    {
        $styles = implode("\n", $this->styles);
        $this->styles = [];
        return $styles;
    }

    /**
     * Returns the page the given form element is part of.
     *
     * @param FormElementInterface $element The element to return the page of
     * @return Page|null The page of the given form element, if any
     */
    protected function getPageFromElement(FormElementInterface $element): ?Page
    {
        $renderable = $element;
        while ($renderable instanceof RenderableInterface && !($renderable instanceof Page)) {
            $renderable = $renderable->getParentRenderable();
        }
        return $renderable;
    }
}