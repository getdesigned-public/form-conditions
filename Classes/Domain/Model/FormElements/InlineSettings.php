<?php

declare(strict_types=1);
namespace Getdesigned\FormConditions\Domain\Model\FormElements;

/*
 * Copyright notice
 *
 * (c) 2025 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormElements\AbstractFormElement;

/**
 * Renders a script tag providing additional inline settings.
 *
 * @since 12.1.0
 * @author Daniel Haring <dh@getdesigned.at>
 * @package Form Conditions
 */
class InlineSettings extends AbstractFormElement
{
    /**
     * The namespace to use.
     *
     * @var string
     */
    protected string $namespace = '';

    /**
     * The settings to apply.
     *
     * @var array
     */
    protected array $settings = [];

    /**
     * The styles to apply.
     *
     * @var string
     */
    protected string $styles = '';

    /**
     * Returns the namespace to use.
     *
     * @return string The namespace to use
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * Sets the namespace to use.
     *
     * @param string $namespace The namespace to set
     * @return $this Current instance for method chaining
     */
    public function setNamespace(string $namespace): self
    {
        $this->namespace = $namespace;
        return $this;
    }

    /**
     * Returns the settings to apply.
     *
     * @return array The settings to apply
     */
    public function getSettings(): array
    {
        return $this->settings;
    }

    /**
     * Sets the settings to apply.
     *
     * @param array $settings The settings to set
     * @return $this Current instance for method chaining
     */
    public function setSettings(array $settings): self
    {
        $this->settings = $settings;
        return $this;
    }

    /**
     * Returns the styles to apply.
     *
     * @return string The styles to apply
     */
    public function getStyles(): string
    {
        return $this->styles;
    }

    /**
     * Sets the styles to apply.
     *
     * @param string $styles The styles to set
     * @return self Current instance for method chaining
     */
    public function setStyles(string $styles): self
    {
        $this->styles = $styles;
        return $this;
    }

    /**
     * Returns the rendered JavaScript providing the settings.
     *
     * @return string The rendered JavaScript
     */
    public function getJavaScript(): string
    {
        $path = GeneralUtility::trimExplode('.', $this->namespace, true);

        $settings = ArrayUtility::setValueByPath([], $path, $this->settings);
        unset($path);

        return sprintf(
            '<script>/*<![CDATA[*/var TYPO3=TYPO3||{};TYPO3.settings=Object.assign(TYPO3.settings||{},Object.fromEntries(Object.entries(%1$s)))/*]]>*/</script>',
            json_encode($settings)
        );
    }

    /**
     * Returns the rendered styles.
     *
     * @return string The rendered styles
     */
    public function getStylesheet(): string
    {
        if (empty($this->styles)) {
            return '';
        }

        return sprintf('<style>%1$s</style>', $this->styles);
    }
}