<?php

declare(strict_types=1);
namespace Getdesigned\FormConditions\EventListener;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\FormConditions\Domain\Model\FormElements\InlineSettings;
use Getdesigned\FormConditions\Service\FormConditionManager;
use Getdesigned\FormConditions\Utility\FormUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormDefinition;
use TYPO3\CMS\Form\Domain\Model\FormElements\AbstractSection;
use TYPO3\CMS\Form\Domain\Model\FormElements\FormElementInterface;
use TYPO3\CMS\Form\Domain\Model\FormElements\Page;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime\Lifecycle\AfterFormStateInitializedInterface;

/**
 * Event listener for toggling renderables according to field values.
 *
 * @since 0.1.0
 * @author Daniel Haring <dh@getdesigned.at>
 * @package Form Conditions
 */
class ConditionalFormRenderable implements AfterFormStateInitializedInterface
{
    /**
     * @inheritdoc
     */
    public function afterFormStateInitialized(FormRuntime $formRuntime): void
    {
        foreach ($formRuntime->getFormDefinition()->getRenderablesRecursively() as $renderable) {
            if ($renderable instanceof FormElementInterface && !FormUtility::checkCondition($renderable, $formRuntime)) {
                FormUtility::disableValidationForElement($renderable, $formRuntime);
            }
        }
    }

    /**
     * Processes a single renderable right before it is rendered.
     *
     * @param FormRuntime $formRuntime The corresponding form runtime
     * @param FormDefinition|AbstractSection|FormElementInterface $renderable The renderable to process
     * @return void
     */
    public function beforeRendering(FormRuntime $formRuntime, $renderable): void
    {
        if (!$renderable instanceof Page) {
            return;
        }

        $conditionManager = GeneralUtility::makeInstance(FormConditionManager::class);

        foreach ($formRuntime->getFormDefinition()->getRenderablesRecursively() as $formElement) {
            if ($formElement instanceof FormElementInterface && $formElement->isEnabled()) {
                $conditionManager->addElement($formElement, $formRuntime);
            }
        }

        $inlineSettings = GeneralUtility::makeInstance(InlineSettings::class, 'tx-form-conditions', 'InlineSettings')
            ->setSettings($conditionManager->flushInlineSettings())
            ->setStyles($conditionManager->flushInlineStyles())
        ;
        $renderable->addElement($inlineSettings);
    }
}