<?php

declare(strict_types=1);
namespace Getdesigned\FormConditions\Utility;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Form\Domain\Model\FormDefinition;
use TYPO3\CMS\Form\Domain\Model\FormElements\FormElementInterface;
use TYPO3\CMS\Form\Domain\Model\Renderable\CompositeRenderableInterface;
use TYPO3\CMS\Form\Domain\Runtime\FormRuntime;
use TYPO3\CMS\Form\Exception;

/**
 * Static utility functions aiding in managing forms.
 *
 * @since 0.1.0
 * @author Daniel Haring <dh@getdesigned.at>
 * @package Form Conditions
 */
class FormUtility
{
    /**
     * Checks the conditions of the given form element.
     *
     * @param FormElementInterface $element The element to check the condition of
     * @param FormRuntime $formRuntime The form runtime holding current values
     * @return bool TRUE if the condition is met and the element should be enabled, otherwise FALSE
     * @throws Exception
     */
    public static function checkCondition(FormElementInterface $element, FormRuntime $formRuntime): bool
    {
        if (
            !$element->isEnabled()
            || empty($enableCondition = (string)($element->getProperties()['enableCondition'] ?? ''))
        ) {
            return true;
        }

        $hasEmptyComposite = false;

        foreach (GeneralUtility::trimExplode(';', $enableCondition, true) as $composite) {

            $isValid = null;
            $isEmptyComposite = true;

            foreach (GeneralUtility::trimExplode(',', $composite, true) as $condition) {
                if (!preg_match('/^(?<identifier>[^!=<>]+)(?<operator>[!=<>]+)?(?<value>.*)$/', $condition, $cmp)) {
                    continue;
                }
                $elementValue = $formRuntime->getRequest()->hasArgument($cmp['identifier'])
                    ? $formRuntime->getRequest()->getArgument($cmp['identifier'])
                    : $formRuntime->getElementValue($cmp['identifier'])
                ;
                if (null === $elementValue) {
                    $isValid = false;
                    continue;
                }
                $isEmptyComposite = false;
                if (null === $isValid) {
                    $isValid = true;
                }
                if (!static::compare($elementValue, $cmp['operator'] ?: null, $cmp['value'] ?? null)) {
                    $isValid = false;
                    break;
                }
            }

            if ($isValid) {
                return true;
            }
            if ($isEmptyComposite) {
                $hasEmptyComposite = true;
            }

        }

        return $hasEmptyComposite;
    }

    /**
     * Disables validation of the given form element and all its descendants.
     *
     * @param FormElementInterface $element The form element to disable validation for
     * @param FormRuntime $formRuntime The corresponding form runtime
     * @return void
     */
    public static function disableValidationForElement(FormElementInterface $element, FormRuntime $formRuntime): void
    {
        $formRuntime->getFormDefinition()
            ->getProcessingRule($element->getIdentifier())
            ->removeAllValidators()
        ;

        if ($element instanceof CompositeRenderableInterface) {
            foreach ($element->getRenderablesRecursively() as $renderable) {
                $formRuntime->getFormDefinition()
                    ->getProcessingRule($renderable->getIdentifier())
                    ->removeAllValidators()
                ;
            }
        }
    }

    /**
     * Recursively returns all renderables of the given form element.
     *
     * @param mixed $element The form element of which to return all renderables of
     * @param FormRuntime $formRuntime The corresponding form runtime
     * @return array All renerables of the given form element
     */
    public static function getRenderablesRecursively($element, FormRuntime $formRuntime): array
    {
        $renderables = [];

        if ($element instanceof FormDefinition) {
            $elements = $element->getPages();
        } elseif (method_exists($element, 'getElements')) {
            $elements = $element->getElements();
        } else {
            return $renderables;
        }

        foreach ($elements as $renderable) {
            if ($renderable instanceof FormElementInterface && !static::checkCondition($renderable, $formRuntime)) {
                continue;
            }
            $renderables = array_merge(
                $renderables,
                [$renderable],
                static::getRenderablesRecursively($renderable, $formRuntime)
            );
        }

        return $renderables;
    }

    /**
     * Performs a comparison of two values.
     *
     * @param mixed $value The value to compare
     * @param string|null $operator The operator to apply – if omitted, a simple empty() check will be performed
     * @param string|null $comparison The comparison value
     * @return bool TRUE if $value meets the given criteria, otherwise FALSE
     * @throws Exception
     */
    protected static function compare($value, string $operator = null, string $comparison = null): bool
    {
        if (null === $operator) {
            return !empty($value);
        }

        // @todo: convert to string (see: \TYPO3\CMS\Form\ViewHelpers\RenderFormValueViewHelper::processElementValue)
        if (!is_string($value) && !is_numeric($value)) {
            return false;
        }

        switch ($operator) {
            case '=':
            case '==':
            case '===':
            case 'eq':
                if (null === $comparison) {
                    return !empty($value);
                }
                return $value == $comparison;
            case '<>':
            case '!=':
            case '!==':
            case 'neq':
                if (null === $comparison) {
                    return empty($value);
                }
                return $value != $comparison;
            case '>':
            case 'gt':
                return $value > ($comparison ?? 0);
            case '>=':
            case 'gte':
                return $value >= ($comparison ?? 0);
            case '<':
            case 'lt':
                return $value < ($comparison ?? 0);
            case '<=':
            case 'lte':
                return $value <= ($comparison ?? 0);
        }

        throw new Exception('Illegal operator "' . $operator . '" found in form element condition: "' . $value . ($operator ?? '') . ($comparison ?? '') . '".', 1676638313);
    }
}