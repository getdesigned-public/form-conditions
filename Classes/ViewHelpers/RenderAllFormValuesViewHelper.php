<?php

declare(strict_types=1);
namespace Getdesigned\FormConditions\ViewHelpers;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Closure;
use Getdesigned\FormConditions\Utility\FormUtility;
use TYPO3\CMS\Form\ViewHelpers\RenderFormValueViewHelper;
use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;

/**
 * Renders all values of a form.
 *
 * @deprecated since 12.1.0
 * @since 0.1.0
 * @author Daniel Haring <dh@getdesigned.at>
 * @package Form Conditions
 */
class RenderAllFormValuesViewHelper
{
    /**
     * @inheritdoc
     */
    public static function renderStatic(
        array $arguments,
        Closure $renderChildrenClosure,
        RenderingContextInterface $renderingContext
    ): string {
        $output = '';
        $as = $arguments['as'];
        $renderable = $arguments['renderable'];

        $formRuntime = $renderingContext
            ->getViewHelperVariableContainer()
            ->get(\TYPO3\CMS\Form\ViewHelpers\RenderRenderableViewHelper::class, 'formRuntime')
        ;

        foreach (FormUtility::getRenderablesRecursively($renderable, $formRuntime) as $element) {
            $output .= RenderFormValueViewHelper::renderStatic(
                [
                    'renderable' => $element,
                    'as' => $as
                ],
                $renderChildrenClosure,
                $renderingContext
            );
        }

        return $output;
    }
}