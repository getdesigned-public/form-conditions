/**
 * Static utility functions for handling settings.
 */
export default {

    /**
     * Returns a settings value by its path.
     *
     * @param {string} path The path to return the settings value for
     * @param {*} [defaultValue] The value to return if the settings path doesn't exist
     * @return {*} The settings value if the path exists, otherwise the default value specified
     */
    get(path, defaultValue) {
        if ('string' !== typeof path) {
            throw new Error(`Argument 1 of SettingsUtility.get() has to be of type string, "${typeof path}" given.`);
        }
        if (!('TYPO3' in window) || !('settings' in TYPO3)) {
            if ('undefined' === typeof defaultValue) {
                throw new Error(`No settings given.`);
            }
            return defaultValue;
        }
        let scope = TYPO3.settings, segments = [];
        path.split('.').forEach(segment => {
            segments.push(segment);
            if ('object' !== typeof scope || !(segment in scope) || 'undefined' === typeof scope[segment]) {
                if ('undefined' === typeof defaultValue) {
                    throw new Error(`Path "${segments.join('.')}" not found within settings.`);
                }
                return defaultValue;
            }
            scope = scope[segment];
        });
        return scope;
    }
};