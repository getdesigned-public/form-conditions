import SettingsUtility from "../Utility/SettingsUtility";

/**
 * Checks form conditions and shows/hides elements accordingly.
 *
 * @constructor
 */
function ConditionMatcher() {

    /**
     * Main entry method.
     */
    this.run = () => {
        if ('interactive' === document.readyState || 'complete' === document.readyState) {
            this.initialize();
        } else {
            document.addEventListener('DOMContentLoaded', this.initialize, {once: true});
        }
    };

    /**
     * Initializes all form conditions for the given scope.
     *
     * @param {HTMLElement} [scope] The scope where to initialize the conditions.
     *                              If omitted, the whole document will be initialized
     */
    this.initialize = scope => {
        if (scope instanceof Event) {
            scope = scope.currentTarget;
        }
        if ('undefined' === typeof scope) {
            scope = document;
        } else if (!(scope instanceof HTMLElement) && scope !== document) {
            throw new Error(`Argument 1 of conditionMatcher.initialize has to be an instance of HTMLElement, "${typeof scope}" given.`);
        }
        const compositeConditions = SettingsUtility.get('ext.form_conditions.cond', {});
        for (let targetIdentifier in compositeConditions) {
            if (!compositeConditions.hasOwnProperty(targetIdentifier)) {
                continue;
            }
            const target = scope.querySelector(`.form-group:has(#${targetIdentifier})`);
            if (!target) {
                continue;
            }
            let conditionClosure = () => {
                compositeConditions[targetIdentifier].every(composite => {
                    let isValid = true;
                    for (let sourceIdentifier in composite) {
                        if (!composite.hasOwnProperty(sourceIdentifier)) {
                            continue;
                        }
                        let source = scope.querySelector(`input[id^="${sourceIdentifier}"][name],textarea[id^="${sourceIdentifier}"][name],select[id^="${sourceIdentifier}"][name],button[id^="${sourceIdentifier}"][name]`), sourceValue;
                        if (source) {
                            sourceValue = _getElementValue(source.form.elements[source.name]);
                        } else {
                            sourceValue = SettingsUtility.get(`ext.form_conditions.val.${sourceIdentifier}`, null);
                        }
                        if (!_compare(
                            sourceValue,
                            composite[sourceIdentifier]['operator'],
                            composite[sourceIdentifier]['comparison']
                        )) {
                            isValid = false;
                            break;
                        }
                    }
                    const formElements = target.querySelectorAll('button[name],input[name],select[name],textarea[name]');
                    // @todo: dispatch events for controlling toggle mechanism
                    if (isValid) {
                        const event = new Event('enabled.cond.element');
                        target.classList.add('tx-form-conditions-display');
                        [...formElements].forEach(formElement => {
                            formElement.removeAttribute('disabled');
                            formElement.dispatchEvent(event);
                        });
                        return false; // break
                    }
                    const event = new Event('disabled.cond.element');
                    target.classList.remove('tx-form-conditions-display');
                    [...formElements].forEach(formElement => {
                        formElement.disabled = 'disabled';
                        formElement.dispatchEvent(event);
                    });
                    return true; // continue
                });
            };
            compositeConditions[targetIdentifier].forEach(composite => {
                for (let sourceIdentifier in composite) {
                    if (!composite.hasOwnProperty(sourceIdentifier)) {
                        continue;
                    }
                    const sources = scope.querySelectorAll(`input[id^="${sourceIdentifier}"][name],textarea[id^="${sourceIdentifier}"][name],select[id^="${sourceIdentifier}"][name],button[id^="${sourceIdentifier}"][name]`);
                    [...sources].forEach(source => {
                        _registerInputChangeEvent(source, conditionClosure);
                    });
                }
            });
            conditionClosure();
        }
    };

    /**
     * Returns the value of the given form element.
     *
     * @param {RadioNodeList|HTMLElement} element The form element of which to return the value of
     * @return {*} The value of the given form element
     * @private
     */
    function _getElementValue(element) {
        if (element instanceof RadioNodeList) {
            for (let index = element.length; index-->0;) {
                if (
                    'input' !== element[index].tagName.toLowerCase()
                    || ('checkbox' !== element[index].type && 'radio' !== element[index].type)
                ) {
                    return element[index].value;
                }
                if (element[index].checked) {
                    return element[index].value;
                }
            }
            return null;
        }
        if (!(element instanceof HTMLElement)) {
            throw new Error(`Argument 1 of ConditionMatcher._getElementValue() has to be either an instance of RadioNodeList or HTMLElement, "${typeof element}" given.`);
        }
        return element.value;
    }

    /**
     * Registers a change event for the given form input element.
     *
     * @param {HTMLElement} element The element to register the change event for
     * @param {function} callback The function to call as soon as the form input got changed
     * @private
     */
    function _registerInputChangeEvent(element, callback) {
        if (!(element instanceof HTMLElement)) {
            throw new Error(`Argument 1 of ConditionMatcher._registerInputChangeEvent() has to be an instance of HTMLElement, "${typeof element}" given.`);
        }
        if ('function' !== typeof callback) {
            throw new Error(`Argument 2 of ConditionMatcher._registerInputChangeEvent() has to be of type function, "${typeof callback}" given.`);
        }
        switch (element.tagName.toLowerCase()) {
            case 'button':
                element.addEventListener('click', callback);
                break;
            case 'input':
                switch (element.type) {
                    case 'check':
                    case 'hidden':
                    case 'radio':
                        element.addEventListener('change', callback);
                        break;
                    case 'submit':
                        element.addEventListener('click', callback);
                        break;
                    default:
                        element.addEventListener('input', callback);
                        break;
                }
                break;
            case 'select':
                element.addEventListener('change', callback);
                break;
            case 'textarea':
                element.addEventListener('input', callback);
                break;
        }
    }

    /**
     * Performs a comparison of two values.
     *
     * @param {*} value The value to compare
     * @param {string} [operator] The operator to apply – if omitted, a simple empty() check will be performed
     * @param {string|number} [comparison] The comparison value
     * @return {boolean} TRUE if $value meets the given criteria, otherwise FALSE
     * @private
     */
    function _compare(value, operator, comparison) {
        if ('undefined' === typeof operator) {
            return !!value;
        }

        if ('string' !== typeof operator) {
            throw new Error(`Argument 2 of ConditionMatcher._compare() has to be of type string, "${typeof operator}" given,`);
        }

        // @todo: convert to string (see: \TYPO3\CMS\Form\ViewHelpers\RenderFormValueViewHelper::processElementValue)
        if ('string' !== typeof value && 'number' !== typeof value) {
            return false;
        }

        switch (operator) {
            case '=':
            case '==':
            case '===':
            case 'eq':
                if ('undefined' === typeof comparison) {
                    return !!value;
                }
                return value == comparison;
            case '<>':
            case '!=':
            case '!==':
            case 'neq':
                if ('undefined' === typeof comparison) {
                    return !value;
                }
                return value != comparison;
            case '>':
            case 'gt':
                if ('undefined' === typeof comparison) {
                    comparison = 0;
                }
                return value > comparison;
            case '>=':
            case 'gte':
                if ('undefined' === typeof comparison) {
                    comparison = 0;
                }
                return value >= comparison;
            case '<':
            case 'lt':
                if ('undefined' === typeof comparison) {
                    comparison = 0;
                }
                return value < comparison;
            case '<=':
            case 'lte':
                if ('undefined' === typeof comparison) {
                    comparison = 0;
                }
                return value <= comparison;
        }

        throw new Error(`Illegal operator "${operator}" found in form element condition.`);
    }

}

export default ConditionMatcher;