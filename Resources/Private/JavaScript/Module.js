import ConditionMatcher from "./Form/ConditionMatcher";

/**
 * The JavaScript module.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 * @since 0.1.0
 */
const matcher = new ConditionMatcher();
matcher.run();
export default matcher;